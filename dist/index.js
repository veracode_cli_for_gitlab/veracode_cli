/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 681:
/***/ (function(__unused_webpack_module, exports, __nccwpck_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.executeCommandExec = void 0;
const child_process_1 = __nccwpck_require__(81);
function executeCommandExec(veracode, command) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            var _a;
            if (veracode.DEBUG == 'true') {
                console.log('### DEBUG LOG ###: Command to run: ' + command);
            }
            const child = (0, child_process_1.exec)(command);
            let output = '';
            (_a = child.stdout) === null || _a === void 0 ? void 0 : _a.on('data', (data) => {
                output += data;
                if (veracode.DEBUG == 'true') {
                    console.log('### DEBUG LOG ###: ' + data);
                }
            });
            child.on('close', (code) => {
                resolve([output, code]);
            });
        });
    });
}
exports.executeCommandExec = executeCommandExec;


/***/ }),

/***/ 849:
/***/ (function(__unused_webpack_module, exports, __nccwpck_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const run_exec_command_1 = __nccwpck_require__(681);
function run_app_scanning(veracode) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        console.log('Running static scanning');
        if (veracode.DEBUG == 'true') {
            console.log('### DEBUG LOG ### veracode vars at app_controller.ts \n' + JSON.stringify(veracode));
        }
        if (veracode.TYPE == 'quick') {
            console.log('run pipeline scan (quick)');
            //build the pipeline scan command
            if (veracode.SOURCE == "") {
                console.log('ERROR: NO SOURCE DEFINED - WILL EXIT');
                process.exit(1);
            }
            if (veracode.POLICY != "") {
                console.log('Download Policy file first');
                var output = yield (0, run_exec_command_1.executeCommandExec)(veracode, 'java -jar ./tools/pipeline-scan.jar -vid ' + veracode.VID + ' -vkey ' + veracode.VKEY + ' --request_policy "' + veracode.POLICY + '"');
                if (veracode.DEBUG == 'true') {
                    console.log('### DEBUG LOG ### Command finished with exit code ' + output[1]);
                }
                console.log('Output of the command in app_controller.ts: \n' + output[0]);
            }
            //run the pipeline scan command
            console.log('Run the pipeline scan (quick)');
            var command = 'java -jar ./tools/pipeline-scan.jar';
            command += ' -vid ' + veracode.VID;
            command += ' -vkey ' + veracode.VKEY;
            command += ' -f ' + veracode.SOURCE;
            command += ' -id true';
            command += ' -jf results.json';
            command += ' -fjf filtered_results.json';
            command += ' -p ' + veracode.PROFILE_NAME;
            if (veracode.POLICY != "") {
                command += ' -pf ./' + ((_a = veracode.POLICY) === null || _a === void 0 ? void 0 : _a.replace(" ", "_")) + '.json';
            }
            if (veracode.BASELINE_FILE) {
                command += ' -bf ' + veracode.BASELINE_FILE;
            }
            if (veracode.DEBUG == 'true') {
                console.log('### DEBUG LOG ### Command to run: ' + command);
            }
            var output = yield (0, run_exec_command_1.executeCommandExec)(veracode, command);
            if (veracode.DEBUG == 'true') {
                console.log('### DEBUG LOG ### Command finished with exit code ' + output[1]);
            }
            var replacedString = output[0].replace(/CWE-/g, "\nCWE-");
            replacedString = replacedString.replace(/Details:/g, "Details:\n");
            replacedString = replacedString.replace(/ \<span\>/g, "");
            replacedString = replacedString.replace(/\<span\>/g, "");
            replacedString = replacedString.replace(/\<\/span\>/g, "\n");
            replacedString = replacedString.replace(/\<a\ href\=\"/g, "\n");
            replacedString = replacedString.replace(/\">/g, "");
            replacedString = replacedString.replace(/\<\/a\>/g, "");
            console.log('Output of the command in app_controller.ts: \n' + replacedString);
        }
        else if (veracode.TYPE == 'branch') {
            console.log('run sandbox scan (branch)');
        }
        else if (veracode.TYPE == 'compliance') {
            console.log('run policy scan (compliance)');
        }
    });
}
exports["default"] = run_app_scanning;


/***/ }),

/***/ 44:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
function run_archive_scan(veracode) {
    console.log('Running archive scanning');
    console.log(JSON.stringify(veracode));
}
exports["default"] = run_archive_scan;


/***/ }),

/***/ 530:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
function run_directory_scan(veracode) {
    console.log('Running directory scanning');
    console.log(JSON.stringify(veracode));
}
exports["default"] = run_directory_scan;


/***/ }),

/***/ 426:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
function run_image_scanning(veracode) {
    console.log('Running iamge scanning');
    console.log(JSON.stringify(veracode));
}
exports["default"] = run_image_scanning;


/***/ }),

/***/ 994:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
function run_repo_scan(veracode) {
    console.log('Running repo scanning');
    console.log(JSON.stringify(veracode));
}
exports["default"] = run_repo_scan;


/***/ }),

/***/ 726:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
function run_sca_scanning(veracode) {
    console.log('Running SCA scanning');
    console.log(JSON.stringify(veracode));
}
exports["default"] = run_sca_scanning;


/***/ }),

/***/ 582:
/***/ (function(__unused_webpack_module, exports, __nccwpck_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const app_controller_1 = __importDefault(__nccwpck_require__(849));
const sca_controller_1 = __importDefault(__nccwpck_require__(726));
const image_controller_1 = __importDefault(__nccwpck_require__(426));
const directory_controller_1 = __importDefault(__nccwpck_require__(530));
const repo_controller_1 = __importDefault(__nccwpck_require__(994));
const archive_controller_1 = __importDefault(__nccwpck_require__(44));
const CLI_VERSION = '0.0.1';
console.log('\n\nVeracode CLI for GitLab version ' + CLI_VERSION);
console.log('==============================================');
console.log('');
const myEnv = process.env;
let veracode = {};
if (myEnv['VERACODE_DEBUG'] == 'true') {
    console.log('### DEBUG LOG ### ENV: ' + myEnv);
    console.log('### DEBUG LOG ### ENV: ' + JSON.stringify(myEnv));
}
console.log('Veracode CLI for GitLab has identified following commands:');
for (var key in myEnv) {
    if (myEnv['VERACODE_DEBUG'] == 'true') {
        console.log('### DEBUG LOG ### ' + key + ' -> ' + myEnv[key]);
    }
    if (key.startsWith('VERACODE_')) {
        var myKeyName = key.split('VERACODE_');
        veracode[myKeyName[1]] = myEnv[key];
        if (myEnv['VERACODE_DEBUG'] == 'true') {
            console.log('### DEBUG LOG ### ' + key + ' -> ' + myEnv[key]);
            console.log('### DEBUG LOG ### Key name -> ' + myKeyName[1]);
            console.log('### DEBUG LOG ### veracode object creation: ' + JSON.stringify(veracode));
        }
    }
}
console.log(JSON.stringify(veracode));
if (veracode.COMMAND == 'scan') {
    console.log('move to scanning');
    if (veracode.TARGET == 'app') {
        console.log('starting static scanning');
        (0, app_controller_1.default)(veracode);
    }
    else if (veracode.TARGET == 'sca') {
        console.log('starting sca scanning');
        (0, sca_controller_1.default)(veracode);
    }
    else if (veracode.TARGET == 'repo') {
        console.log('starting repo scanning');
        (0, repo_controller_1.default)(veracode);
    }
    else if (veracode.TARGET == 'image') {
        console.log('starting image scanning');
        (0, image_controller_1.default)(veracode);
    }
    else if (veracode.TARGET == 'directory') {
        console.log('starting directory scanning');
        (0, directory_controller_1.default)(veracode);
    }
    else if (veracode.TARGET == 'archive') {
        console.log('starting archive scanning');
        (0, archive_controller_1.default)(veracode);
    }
    else if (veracode.TARGET == '') {
        console.log('ERROR: NO SCAN TARGET DEFINED - WILL EXIT');
        process.exit(1);
    }
}
else if (veracode.COMMAND == '') {
    console.log('ERROR: NO COMMAND DEFINED - WILL EXIT');
    process.exit(1);
}


/***/ }),

/***/ 81:
/***/ ((module) => {

module.exports = require("child_process");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __nccwpck_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			__webpack_modules__[moduleId].call(module.exports, module, module.exports, __nccwpck_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete __webpack_module_cache__[moduleId];
/******/ 		}
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat */
/******/ 	
/******/ 	if (typeof __nccwpck_require__ !== 'undefined') __nccwpck_require__.ab = __dirname + "/";
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __nccwpck_require__(582);
/******/ 	module.exports = __webpack_exports__;
/******/ 	
/******/ })()
;