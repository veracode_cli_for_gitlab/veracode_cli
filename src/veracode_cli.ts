import { env } from "process";
import { veracode } from "./interfaces/veracode_interface";
import run_app_scanning from "./scan/app/app_controller"
import run_sca_scanning from "./scan/sca/sca_controller";
import run_image_scanning from "./scan/image/image_controller";
import run_directory_scan from "./scan/directory/directory_controller";
import run_repo_scan from "./scan/repo/repo_controller";
import run_archive_scan from "./scan/archive/archive_controller";

const CLI_VERSION = '0.0.1'
console.log('\n\nVeracode CLI for GitLab version '+CLI_VERSION)
console.log('==============================================')
console.log('')

const myEnv:any = process.env
let veracode:any = {}

if ( myEnv['VERACODE_DEBUG'] == 'true'){
    console.log('### DEBUG LOG ### ENV: '+myEnv)
    console.log('### DEBUG LOG ### ENV: '+JSON.stringify(myEnv))
}

console.log('Veracode CLI for GitLab has identified following commands:')


for (var key in myEnv) {
    if ( myEnv['VERACODE_DEBUG'] == 'true' ){
        console.log('### DEBUG LOG ### '+key+' -> '+myEnv[key]);
    }
    if ( key.startsWith('VERACODE_') ){
        var myKeyName = key.split('VERACODE_')
        veracode[myKeyName[1]] = myEnv[key]
        if ( myEnv['VERACODE_DEBUG'] == 'true'){
            console.log('### DEBUG LOG ### '+key+' -> '+myEnv[key]);
            console.log('### DEBUG LOG ### Key name -> '+myKeyName[1])
            console.log('### DEBUG LOG ### veracode object creation: '+JSON.stringify(veracode))
        }
    }
}
console.log(JSON.stringify(veracode))


if ( veracode.COMMAND == 'scan' ){
    console.log('move to scanning')
    if ( veracode.TARGET == 'app' ){
        console.log('starting static scanning')
        run_app_scanning(veracode)
    }
    else if ( veracode.TARGET == 'sca' ){
        console.log('starting sca scanning')
        run_sca_scanning(veracode)
    }
    else if ( veracode.TARGET == 'repo' ){
        console.log('starting repo scanning')
        run_repo_scan(veracode)
    }
    else if ( veracode.TARGET == 'image' ){
        console.log('starting image scanning')
        run_image_scanning(veracode)
    }
    else if ( veracode.TARGET == 'directory' ){
        console.log('starting directory scanning')
        run_directory_scan(veracode)
    }
    else if ( veracode.TARGET == 'archive' ){
        console.log('starting archive scanning')
        run_archive_scan(veracode)
    }
    else if ( veracode.TARGET == '' ){
        console.log('ERROR: NO SCAN TARGET DEFINED - WILL EXIT')
        process.exit(1)
    }
}
else if ( veracode.COMMAND == '' ){
    console.log('ERROR: NO COMMAND DEFINED - WILL EXIT')
    process.exit(1)
}

