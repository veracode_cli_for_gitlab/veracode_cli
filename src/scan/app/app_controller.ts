import { veracode } from "../../interfaces/veracode_interface";
import { executeCommandExec } from "../../run_exec_command"

export default async function run_app_scanning(veracode: veracode){
    console.log('Running static scanning')
    if ( veracode.DEBUG == 'true'){
        console.log('### DEBUG LOG ### veracode vars at app_controller.ts \n'+JSON.stringify(veracode))
    }
    if ( veracode.TYPE == 'quick' ){
        console.log('run pipeline scan (quick)')

        //build the pipeline scan command
        if ( veracode.SOURCE == "" ) {
            console.log('ERROR: NO SOURCE DEFINED - WILL EXIT')
            process.exit(1)
        }
        
        if ( veracode.POLICY != "" ){
            console.log('Download Policy file first')
            var output = await executeCommandExec(veracode,'java -jar ./tools/pipeline-scan.jar -vid '+veracode.VID+' -vkey '+veracode.VKEY+' --request_policy "'+veracode.POLICY+'"')
            if ( veracode.DEBUG == 'true'){
                console.log('### DEBUG LOG ### Command finished with exit code '+output[1])
            }
            console.log('Output of the command in app_controller.ts: \n'+output[0])
        }

        //run the pipeline scan command
        console.log('Run the pipeline scan (quick)')
        var command = 'java -jar ./tools/pipeline-scan.jar'
        command += ' -vid '+veracode.VID
        command += ' -vkey '+veracode.VKEY
        command += ' -f '+veracode.SOURCE
        command += ' -id true'
        command += ' -jf results.json'
        command += ' -fjf filtered_results.json'
        command += ' -p '+veracode.PROFILE_NAME
        if ( veracode.POLICY != "" ){
            command += ' -pf ./'+veracode.POLICY?.replace(" ","_")+'.json'
        }
        if ( veracode.BASELINE_FILE ){
            command += ' -bf '+veracode.BASELINE_FILE
        }
        if ( veracode.DEBUG == 'true'){
            console.log('### DEBUG LOG ### Command to run: '+command)
        }
        var output = await executeCommandExec(veracode,command)
        if ( veracode.DEBUG == 'true'){
            console.log('### DEBUG LOG ### Command finished with exit code '+output[1])
        }
        var replacedString = output[0].replace(/CWE-/g,"\nCWE-")
        replacedString = replacedString.replace(/Details:/g,"Details:\n")
        replacedString = replacedString.replace(/ \<span\>/g,"")
        replacedString = replacedString.replace(/\<span\>/g,"")
        replacedString = replacedString.replace(/\<\/span\>/g,"\n")
        replacedString = replacedString.replace(/\<a\ href\=\"/g,"\n")
        replacedString = replacedString.replace(/\">/g,"")
        replacedString = replacedString.replace(/\<\/a\>/g,"")
        console.log('Output of the command in app_controller.ts: \n'+replacedString)


    }
    else if ( veracode.TYPE == 'branch'){
        console.log('run sandbox scan (branch)')
    }
    else if ( veracode.TYPE == 'compliance' ){
        console.log('run policy scan (compliance)')
    }

}