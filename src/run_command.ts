import { spawn } from 'child_process';

export function executeCommand(command: string, args: string[]): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    console.log('Command to run: '+command+args)
    const childProcess = spawn(command, args, {
        detached: true
    })

    let output = '';

    childProcess.stdout.on('data', (data: string | Buffer) => {
      //output += data.toString();
      output = data.toString();
      console.log(output)
    });

    childProcess.stderr.on('data', (data: string | Buffer) => {
      reject(data.toString());
    });

    childProcess.on('exit', (code: number) => {
      if (code !== 0) {
        reject(`Command ${command} failed with code ${code}`);
      }
      resolve(output);
    });
  });
}