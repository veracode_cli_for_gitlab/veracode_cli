import { exec } from 'child_process';
import { veracode } from "./interfaces/veracode_interface";

export async function executeCommandExec(veracode: veracode,command: string): Promise<any> {
  return new Promise<any>((resolve, reject) => {
    if ( veracode.DEBUG == 'true'){
        console.log('### DEBUG LOG ###: Command to run: '+command)
    }
    const child = exec(command);

    let output = ''

    child.stdout?.on('data', (data: string) => {
        output += data;
        if ( veracode.DEBUG == 'true' ){
            console.log('### DEBUG LOG ###: '+data);
        }
    });

    child.on('close', (code: number) => {
        resolve([output,code]);
    });
    


  });
}