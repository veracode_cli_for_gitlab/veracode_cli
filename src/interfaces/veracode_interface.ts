export interface veracode {
    //global
    VID:string
    VKEY:string
    SRCCLR_API_TOKEN:string

    //commands and parameters
    COMMAND: string
    TARGET: string
    SOURCE: string
    TYPE: string
    PROFILE_NAME: boolean
    OUTPUT?: string
    CREATE_PROFILE?: string
    BRANCH_NAME?: string
    WAIT_FOR_SCAN_TO_COMPLETE?: string 
    POLICY?: string
    BASELINE_FILE?: string
    BASELINE_GEN?: boolean
    DEBUG: string
}